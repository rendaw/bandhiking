Get hiking at <https://bandhiking.isandrew.com/>!

# Why do you want Bandhiking?

Bandcamp is an awesome indie music publication platform, but it's extremely hard to find new music there.  For one, without continued user focus site playback stops after one song.  This means you can't put the "bandcamp top 40s" on in the background and wait until you hear something that piques your interest.

This is a 3rd party frontend to make it easy to do that, basically.

# Questions

## Why can't I go back to a previous song?

Bandhiking is a music discovery app - it's goal is to force lots of music upon you.  If you're interested enough to listen to a song again we consider it sufficiently discovered.

You can always listen to songs again on Bandcamp itself - find links to previously listened songs in the history and favorites tabs.

We also don't want to abuse our relationship with Bandcamp (who historically have preferred people not to use their free site audio playback features for entertainment, a subtle distinction).

## What's the "all" category?

Bandcamp ranks music per genre and subgenre, but some music is missing either a genre or subgenre and so won't appear in the more-specific listings.  To hear that music you'll need to listen to the "all" category.  Unfortunately there will be overlap with the more specific categories.  This is a technical problem, ideas welcome.