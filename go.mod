module gitlab.com/rendaw/bandhiking

go 1.18

require (
	github.com/Jeffail/gabs/v2 v2.6.1
	github.com/go-resty/resty/v2 v2.7.0
	github.com/jmoiron/sqlx v1.3.5
	github.com/platformsh/config-reader-go/v2 v2.3.1
	github.com/robfig/cron v1.2.0
	github.com/rubenv/sql-migrate v1.1.2
	github.com/sirupsen/logrus v1.9.0
	golang.org/x/text v0.3.7
)

require (
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/go-gorp/gorp/v3 v3.0.2 // indirect
	github.com/gofrs/uuid v4.2.0+incompatible // indirect
	github.com/jackc/chunkreader/v2 v2.0.1 // indirect
	github.com/jackc/pgconn v1.13.0 // indirect
	github.com/jackc/pgio v1.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgproto3/v2 v2.3.1 // indirect
	github.com/jackc/pgservicefile v0.0.0-20200714003250-2b9c44734f2b // indirect
	github.com/jackc/pgtype v1.12.0 // indirect
	github.com/jackc/pgx/v4 v4.17.0 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.3 // indirect
	github.com/mattn/go-sqlite3 v2.0.1+incompatible // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/shopspring/decimal v1.3.1 // indirect
	golang.org/x/crypto v0.0.0-20220824171710-5757bc0c5503 // indirect
	golang.org/x/net v0.0.0-20220822230855-b0a4917ee28c // indirect
	golang.org/x/sys v0.0.0-20220825204002-c680a09ffe64 // indirect
	gopkg.in/gorp.v1 v1.7.2 // indirect
)
